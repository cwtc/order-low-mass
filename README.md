# An order for Low Mass

A liturgical booklet for an Anglo-Catholic style of Solemn High Mass. It reflects the usage of the Church of St. John the Evangelist in Montreal, QC, but is free for use/adaptation by anyone.

Includes:
* Angelus
* Regina Caeli
* Order for Mass